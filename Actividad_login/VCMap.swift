//
//  VCMap.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 20/4/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase

class VCMap: UIViewController,LocationAppDelegate {
    
    @IBOutlet var myMap:MKMapView?
    var pines:[String:MKPointAnnotation] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        LocationApp.locat.delegate=self
        LocationApp.locat.iniciarAct()
        DataHolder.loger.firDataBaseRef.child("Vikingos").observe(FIRDataEventType.value, with: { (snapshot) in
            let arTemp = snapshot.value as? Array<AnyObject>
            DataHolder.loger.arVikingos = Array<Vikingo>()
            for co in arTemp! as [AnyObject] {
                let vikingoi:Vikingo = Vikingo(valores : co as! [String:AnyObject])
                DataHolder.loger.arVikingos?.append(vikingoi)
                
                var coordTemp:CLLocationCoordinate2D = CLLocationCoordinate2D()
                coordTemp.latitude = vikingoi.sLat!
                coordTemp.longitude = vikingoi.sLon!
                self.agregarPin(coordenada: coordTemp, titulo: vikingoi.sNombre!)
            }
        })

        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actualizarLocation(coordenada: CLLocationCoordinate2D) {
        let region: MKCoordinateRegion = MKCoordinateRegionMake(coordenada, MKCoordinateSpanMake(0.05, 0.05))
        myMap?.setRegion(region, animated: true)
    }
    
    func agregarPin(coordenada: CLLocationCoordinate2D,titulo tpin:String) {
        var annotation:MKPointAnnotation = MKPointAnnotation()
        
        if(pines[tpin] != nil) {
            annotation = pines[tpin]! as MKPointAnnotation
            myMap?.removeAnnotation(annotation)
        }
        annotation.coordinate = coordenada
        annotation.title = tpin
        pines[tpin] = annotation
        myMap?.addAnnotation(annotation)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
