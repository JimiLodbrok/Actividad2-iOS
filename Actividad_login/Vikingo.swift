//
//  Vikingo.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 20/4/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit

class Vikingo: NSObject {
    var sNombre:String?
    var sApellido:String?
    var dAltura:Double?
    var sRango:String?
    var sRutaImagen:String?
    var sLon:Double?
    var sLat:Double?

    init(valores:[String:AnyObject]) {
        sNombre=valores["Nombre"] as? String
        sApellido=valores["Apellidos"] as? String
        dAltura=valores["Altura"] as? Double
        sRango=valores["Rango"] as? String
        sRutaImagen=valores["RutaImagen"] as? String
        sLon = valores["lon"] as? Double
        sLat = valores["lat"] as? Double
    }
}
