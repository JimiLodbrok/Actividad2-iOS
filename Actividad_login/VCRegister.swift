//
//  VCRegister.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import FirebaseAuth

class VCRegister: UIViewController {
    
    @IBOutlet var txtUser:UITextField?
    @IBOutlet var txtMail:UITextField?
    @IBOutlet var txtPass:UITextField?
    @IBOutlet var txtRPass:UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registrarse(){
        if (!((txtUser?.text?.isEmpty)!) && !((txtPass?.text?.isEmpty)!) && !((self.txtMail?.text?.isEmpty)!) && !((txtRPass?.text?.isEmpty)!) && (txtPass?.text == txtRPass?.text) && ((txtPass?.text?.characters.count)! > 5)) {
            FIRAuth.auth()?.createUser(withEmail: (txtMail?.text!)!, password: (txtPass?.text!)!) { (user, error) in
                if ((error == nil)) {
                    let alertController = UIAlertController(title: "Correct Register", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK!", style: UIAlertActionStyle.default) {
                        (result : UIAlertAction) -> Void in
                        self.performSegue(withIdentifier: "transitionlog", sender: self)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)

                }
                else {
                    print("ERROR EN REGISTRO: ",error!)
                }
            }
        }else if (txtPass?.text != txtRPass?.text) {
            let alerta = UIAlertController(title: "Upss!!",message:"The passwords dosn´t match", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
            self.present(alerta, animated: true, completion: nil)
        }else if ((txtPass?.text?.characters.count)! < 6) {
            let alerta = UIAlertController(title: "Upss!!",message:"The password have to be minimum 6 characters length", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
            self.present(alerta, animated: true, completion: nil)
        }else {
            let alerta = UIAlertController(title: "Upss!!",message:"There are missing fields  ", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
            self.present(alerta, animated: true, completion: nil)
        }
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
