//
//  TVCCollection.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 28/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
 
class CVCItem2: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var miColeccion:UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (DataHolder.loger.arVikingos==nil) {
            return 0
        }else {
            return  (DataHolder.loger.arVikingos?.count)!
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CVCCell = collectionView.dequeueReusableCell(withReuseIdentifier: "miCelda2", for: indexPath)as! CVCCell
        let vikingoi: Vikingo =  (DataHolder.loger.arVikingos?[indexPath.row])! as Vikingo
        cell.lbl?.text = vikingoi.sNombre
        let ru : String = vikingoi.sRutaImagen!
        DataHolder.loger.downloadImg2(ruta: ru , celda: cell)
        return cell
    }
}
