//
//  LocationApp.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 20/4/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import CoreLocation

class LocationApp: NSObject, CLLocationManagerDelegate {

    var location:CLLocationManager?
    var delegate:LocationAppDelegate?
    static var locat:LocationApp = LocationApp()
    
    override init() {
        super.init()
        location=CLLocationManager()
        location?.delegate=self
        location?.requestAlwaysAuthorization()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.actualizarLocation(coordenada: locations[0].coordinate)
        location?.stopUpdatingLocation()
    }
    
    func iniciarAct() {
        location?.startUpdatingLocation()
    }
}

protocol LocationAppDelegate{
    func actualizarLocation(coordenada: CLLocationCoordinate2D)
}
