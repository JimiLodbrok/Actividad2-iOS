//
//  VCLogin.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import Firebase

class VCLogin: UIViewController, VCLoginDelegate {
    @IBOutlet var txtUser:UITextField?
    @IBOutlet var txtPass:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logearse(){
        self.tryLogin(delegate: self)
    }
    
    func tryLogin(delegate: VCLoginDelegate){
        FIRAuth.auth()?.signIn(withEmail: (txtUser?.text!)!, password: (txtPass?.text!)!) { (user, error) in
            if (error == nil) {
                delegate.login!(delegate: delegate)
            }
            else {
                let alerta = UIAlertController(title: "Login Error",message:"Incorrect User or Password ", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                self.present(alerta, animated: true, completion: nil)
                print("ERROR EN REGISTRO: ",error!)
            }
        }
    }
        
    func datosCargados(){
        self.performSegue(withIdentifier: "transition", sender: self)
    }
        
    func login(delegate: VCLoginDelegate){
        DataHolder.loger.firDataBaseRef.child("Vikingos").observe(FIRDataEventType.value, with: { (snapshot) in
            let arTemp = snapshot.value as? Array<AnyObject>
            if(DataHolder.loger.arVikingos==nil) {
                DataHolder.loger.arVikingos = Array<Vikingo>()
            }
            
            for co in arTemp! as [AnyObject] {
                let vikingoi:Vikingo = Vikingo(valores : co as! [String:AnyObject])
                
                DataHolder.loger.arVikingos?.append(vikingoi)
            }
            delegate.datosCargados!()
        })
    }
}

@objc protocol VCLoginDelegate{
    @objc optional func datosCargados()
    @objc optional func login(delegate: VCLoginDelegate)
}
