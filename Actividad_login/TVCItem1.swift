//
//  TVCcell.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import FirebaseDatabase

class TVCItem1: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var miTabla:UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (DataHolder.loger.arVikingos==nil) {
            return 0
        }else {
            return  (DataHolder.loger.arVikingos?.count)!
        }
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TVCCell = tableView.dequeueReusableCell(withIdentifier: "miCelda", for: indexPath)as! TVCCell
        let vikingoi: Vikingo =  (DataHolder.loger.arVikingos?[indexPath.row])! as Vikingo
        cell.lbl?.text = vikingoi.sNombre
        let ru : String = vikingoi.sRutaImagen!
        DataHolder.loger.downloadImg(ruta: ru , celda: cell)
        return cell
    }
}
