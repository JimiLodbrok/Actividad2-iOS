//
//  DataHolder.swift
//  Actividad_login
//
//  Created by Jaime García Castán on 23/3/17.
//  Copyright © 2017 Jaime García Castán. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class DataHolder: NSObject {
    static let loger:DataHolder = DataHolder()
    
    var location:LocationApp?
    var firDataBaseRef: FIRDatabaseReference!
    var arVikingos:Array<Vikingo>?
    var firStorageRef:FIRStorageReference?
    
    var userName:String?
    var passWord:String?
    var e_mail:String?
    
    func initFirebase() {
        FIRApp.configure()
        firDataBaseRef = FIRDatabase.database().reference()
        firStorageRef = FIRStorage.storage().reference()
    }
   
    func initLocationApp() {
        location=LocationApp()
    }
    
    func downloadImg(ruta:String, celda: TVCCell) {
        firStorageRef?.child(ruta).data(withMaxSize: 1 * 1024 * 1024) { data, error in
            if error != nil {
                
            } else {
                celda.img?.image = UIImage(data: data!)
            }
        }
        
    }
    
    func downloadImg2(ruta:String, celda: CVCCell) {
        firStorageRef?.child(ruta).data(withMaxSize: 1 * 1024 * 1024) { data, error in
            if error != nil {
                
            } else {
                celda.img?.image = UIImage(data: data!)
            }
        }
        
    }
}
